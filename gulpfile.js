var gulp = require('gulp');
const jade = require('gulp-pug');
var sass = require('gulp-sass');
var autoprefixer = require('gulp-autoprefixer');
var concat = require('gulp-concat');
var minifycss = require('gulp-clean-css');
var imagemin = require('gulp-imagemin');
var rename = require('gulp-rename');
var clean = require('gulp-clean');
var notify = require('gulp-notify');
var plumber = require('gulp-plumber');
var sourcemaps = require('gulp-sourcemaps');
var uglify = require('gulp-uglify');
var useref = require('gulp-useref');
var browserSync = require('browser-sync').create();
var gulpSequence = require('gulp-sequence')

// Эта конструкция работает синхронно, сначала выполняется задача 'clean' и только после ее завершнения запускается 'dev'.
gulp.task('default', gulpSequence('clean', 'dev'));

// Аналогично с предыдушей задачей.
gulp.task('production', gulpSequence('clean', 'build'));

gulp.task('dev', gulpSequence('build', 'watch', 'browser-sync'));

// Задача 'build' представляет собой сборку в режиме продакшн.
gulp.task('build', ['pug', 'assets', 'styles', 'scripts']);

gulp.task('pug', function() {
    return gulp.src(['src/*.pug','!src/_*.pug'])
        .pipe(jade({
            pretty: '\t'
        }))
        .pipe(gulp.dest('build/'));
});

gulp.task('images', function() {
    return gulp.src('src/assets/images/**/*')
        .pipe(imagemin())
        .pipe(gulp.dest('build/assets/images'));
});

//Перемешение наших локальных файлов в папку build
gulp.task('moveFiles', function() {
    return gulp.src('src/assets/**/*.*')
        .pipe(gulp.dest('build/assets/'));
});

gulp.task('assets', gulpSequence('images', 'moveFiles'));

// Задача 'styles' выполняет сборку наших стилей.
gulp.task('styles', function() {
    return gulp.src('src/styles/{main,global}.scss')
        .pipe(plumber({ // plumber - плагин для отловли ошибок.
            errorHandler: notify.onError(function(err) { // nofity - представление ошибок в удобном для вас виде.
                return {
                    title: 'Styles',
                    message: err.message
                }
            })
        }))
        .pipe(sourcemaps.init()) //История изменения стилей, которая помогает нам при отладке в devTools.
        .pipe(sass()) //Компиляция sass.
        .pipe(autoprefixer({ //Добавление autoprefixer.
            browsers: ['last 2 versions']
        }))
        .pipe(concat('styles.css')) //Соедение всех файлом стилей в один и задание ему названия 'styles.css'.
        .pipe(minifycss({compatibility: 'ie9'})) //Минификация стилей
        .pipe(sourcemaps.write())
        .pipe(rename('styles.min.css')) //Переименование
        .pipe(gulp.dest('build/styles'));
});

gulp.task('scripts', function() {
    return gulp.src('src/scripts/*.js')
        //.pipe(uglify()) //Минификация скриптов.
        .pipe(gulp.dest('build/scripts'));
});

//Задача для удаления папки build.
gulp.task('clean', function() {
    return gulp.src('build/')
        .pipe(clean());
});

gulp.task('useref', function() {
    return gulp.src('build/index.html')
        .pipe(useref()) //Выполняет объединение файлов в один по указанным в разметке html комментариям.
        .pipe(gulp.dest('build/'));
});

// Задача 'watch' следит за всеми нашими файлами в проекте и при изменении тех или иных перезапустает соответсвующую задачу.
gulp.task('watch', function() {
    gulp.watch('src/styles/**/*.scss', ['styles']); //стили
    gulp.watch('src/scripts/**/*.js', ['scripts']); //скрипты
    gulp.watch('src/**/*.pug', ['pug']).on('change', browserSync.reload); // jade
    gulp.watch('src/assets/**/*.*', ['assets']); //наши локальные файлы(картинки, шрифты)
    gulp.watch('src/**/*.*').on('change', browserSync.reload); //Перезапуск browserSynс
});

//Задача для запуска сервера.
gulp.task('browser-sync', function() {
    return browserSync.init({
        server: {
            baseDir: 'build/'
        }
    });
});

